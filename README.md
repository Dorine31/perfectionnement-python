
# Perfectionnement-python

Exercice de perfectionnement en Python (MOOC openclassrooms.com)
finalité de l'exercice :
- coder proprement
- travailler dans un environnement virtuel
- organiser des projets en modules
- gérer les erreurs
- créer un graphique
- utiliser les méthodes spéciales / décorateurs / générateurs et itérateurs...

projet fil rouge :
Analyser les données de l'assemblée nationale (cf. dossier data)
  -> analyser la parité de chaque parti politique : trouver les informations pertinentes dans un fichier conséquent, les trier et créer des graphiques
