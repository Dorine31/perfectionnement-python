import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns #pimp my matplotlib

import logging as lg
lg.basicConfig(level=lg.DEBUG)

class SetOfParliamentMember: #create SetOfParliamentMember objects
    def __init__(self,name):
        self.name = name

    def __repr__(self):
        return "SetOfParliamentMember: {} members.".format(len(self.dataframe))

    def data_from_csv(self,csv_file): #create a dataframe from a csv file
        self.dataframe = pd.read_csv( csv_file,sep=";")

    def data_from_dataframe(self, dataframe):
        self.dataframe = dataframe

    #create and display a chart
    def display_chart(self):
        data = self.dataframe
        female_mps = data[data.sexe == "F"] #dataframe that contains female column
        male_mps = data[data.sexe == "H"]

        counts = [len(female_mps), len(male_mps)]
        counts = np.array(counts) #use a ndarray object
        nb_mps = counts.sum() #sum the number of lines eg. the total of the members
        proportions = counts / nb_mps

        labels = ["Female ({})".format(counts[0]), "Male ({})".format(counts[1])]

        fig, ax = plt.subplots() #create one chart in one window
        ax.axis("equal") #axis of lines = axis of column
        ax.pie( #the type of the chart
            proportions,
            labels = labels,
            autopct = "%1.1f%%"
        )

        plt.title("{} ({} MPs)".format(self.name, nb_mps)) #add title

        plt.show() #display the chart

    # create the ability to display a chart for each party in split_by_political_party function
    def split_by_political_party(self): #return a dictionary named result : key is a party and value is a dataframe
        result = {}
        data = self.dataframe #get the dataframe

        #create a new dataframe containing the name_of_party column, cleaned
        all_parties = data['parti_ratt_financier'].dropna().unique() #delete the empty fields with dropna, delete the multiple values with unique

        for party in all_parties: #for each party of the dataframe
            data_subset = data[data.parti_ratt_financier == party] #create a mask to filter data for each party
            subset = SetOfParliamentMember('MPs from party "{}"'.format(party)) #create the object ParliamentMember
            subset.data_from_dataframe(data_subset) #create a dataframe from the object
            result[party] = subset #add keys (party) and values (sum of the party's lines) in the dictionary

        return result

# launch the analysis
def launch_analysis(data_file, by_party = False, info = False):
    sopm = SetOfParliamentMember("All MPs")
    sopm.data_from_csv(os.path.join("data",data_file))
    sopm.display_chart()

    if by_party: #if by_party is true, display a chart for each party
        for party, s in sopm.split_by_political_party().items():
            s.display_chart()

    if info: #if info is true, print the informations of the object: Here it's the number of parliament members
        print(sopm)

if __name__ == "__main__":
    launch_analysis('current_mps.csv')
