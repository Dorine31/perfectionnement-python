#! /usr/bin/env python3
# coding: utf-8

import argparse #use argparse package to add arguments

import analysis.csv as c_an
import analysis.xml as x_an

import logging as lg
lg.basicConfig(level=lg.DEBUG)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d","--datafile",help="""CSV file containing pieces of
        information about the members of parliament""")
    parser.add_argument("-e","--extension",help="""Type of file to analyse. Is it a CSV or a XML ?""")
    parser.add_argument("-p","--byparty",action='store_true',help="""displays a graph for each political party""")
    parser.add_argument("-i","--info",action='store_true',help="""information about the file""")

    return parser.parse_args()

def main():
    args = parse_arguments()
    try:
        datafile = args.datafile
        if datafile == None:
            raise Warning('You must indicate a datafile !')
        else:
            try:
                if args.extension == "xml":
                    x_an.launch_analysis(datafile)
                elif args.extension == "csv":
                    c_an.launch_analysis(datafile, args.byparty, args.info)
            except FileNotFoundError as e:
                 lg.warning("Ow :( The file was not found. Here is the original message of the exception : {}".format(e))
            finally:
                lg.info('#################### Analysis is over ######################')
    except Warning as e:
        print(e)

if __name__ == "__main__": #si le script est importé en tant que module, "name" = "nom-du-module"
    main()
